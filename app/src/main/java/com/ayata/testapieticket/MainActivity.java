package com.ayata.testapieticket;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.ayata.testapieticket.Pojomodel.Seatarragmentmodel;
import com.ayata.testapieticket.network.APIClient;
import com.ayata.testapieticket.network.APIService;

import org.json.JSONArray;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    Button btnid;
    APIService retofitinterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnid=findViewById(R.id.btnid);


        OkHttpClient.Builder okhttpclientBuilder=new OkHttpClient.Builder();
        HttpLoggingInterceptor logging=new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        okhttpclientBuilder.addInterceptor(logging);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://kilindar.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okhttpclientBuilder.build()).build();

        retofitinterface=retrofit.create(APIService.class);

        btnid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetData();
            }


        });

    }

    private void GetData()
    {


        Call<Seatarragmentmodel> call_seat= retofitinterface.getSeatAll(2,"2020-12-17","Bearer bf1cb02b21261510a7f70446f4b1ded2a879c3e7");
        call_seat.enqueue(new Callback<Seatarragmentmodel>() {
            @Override
            public void onResponse(Call<Seatarragmentmodel> call, Response<Seatarragmentmodel> response) {

                response.body();
            }

            @Override
            public void onFailure(Call<Seatarragmentmodel> call, Throwable t) {

            }
        });

    }


}