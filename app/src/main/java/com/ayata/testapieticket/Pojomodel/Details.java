package com.ayata.testapieticket.Pojomodel;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

    @SerializedName("selected_seats")
    @Expose
    private String selectedSeats;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("seats")
    @Expose
    private List<Seat> seats = null;
    @SerializedName("tracker_id")
    @Expose
    private String trackerId;

    public String getSelectedSeats() {
        return selectedSeats;
    }

    public void setSelectedSeats(String selectedSeats) {
        this.selectedSeats = selectedSeats;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

    public String getTrackerId() {
        return trackerId;
    }

    public void setTrackerId(String trackerId) {
        this.trackerId = trackerId;
    }

}
