package com.ayata.testapieticket.network;

import com.ayata.testapieticket.Pojomodel.Seatarragmentmodel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface APIService {
    @GET("layout/")
    Call<Seatarragmentmodel> getSeatAll(@Query("bus_id") int bus_id, @Query("date") String date, @Header("Authorization") String header);
}
